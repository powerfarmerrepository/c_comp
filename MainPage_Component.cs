﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

namespace Components
{
  public class MainPage_Component
    {

      private IWebDriver driver;
      WebDriverWait webDriverWait;  
        
      public MainPage_Component(IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
            webDriverWait =  new WebDriverWait(driver, TimeSpan.FromSeconds(5));

        }



        public void search(String searchText) {

            this.inpSearch.SendKeys(searchText);
            this.inpSearch.SendKeys(Keys.Return);
        }

        [FindsBy(How = How.Id, Using = "lst-ib")]
        public IWebElement inpSearch { get; set; }

        [FindsBy(How = How.Name, Using = "btnK")]
        public IWebElement btnSearch { get; set; }

        [FindsBy(How = How.Id, Using = "login")]
        public IWebElement Submit { get; set; }
       
    }
}
